export class Robot {
  id: string;

  price: number;
  name: string;

  static fromObject(object: Object): Robot {
    let robot: Robot = new Robot();

    robot.id = object["id"];
    robot.price = object["price"];
    robot.name = object["name"];

    return robot;
  }
}
