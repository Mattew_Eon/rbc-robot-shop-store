import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Robot} from "../../../model/robot";

@Component({
  selector: "robot-view",
  templateUrl: "robot-view.component.html"
})
export class RobotViewComponent {
  @Input() robot:Robot = new Robot();

  @Output() onDelete: EventEmitter<Robot> = new EventEmitter<Robot>();
}
