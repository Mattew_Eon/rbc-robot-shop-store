import {Component, EventEmitter, Output} from "@angular/core";
import {Robot} from "../../../model/robot";
import {RobotService} from "../services/robot.service";

@Component({
  selector: "robot-create",
  templateUrl: "robot-create.component.html",
})
export class RobotCreateComponent {
  public robot:Robot = new Robot();

  @Output() onCreate: EventEmitter<Robot> = new EventEmitter<Robot>();

  constructor(private robotS: RobotService) {
  }

  addRobot() {
    this.robotS.create(this.robot).subscribe((robotCreated: Robot) => {
      this.robot = new Robot();
      this.onCreate.emit(robotCreated);
    })
  }
}
