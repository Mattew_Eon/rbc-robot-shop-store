import {Component, ViewChild} from "@angular/core";
import {RobotListComponent} from "./robot-list.component";
import {Robot} from "../../../model/robot";

@Component({
  selector: "robot-home",
  templateUrl: "robot-home.component.html"
})
export class RobotHomeComponent {
  @ViewChild(RobotListComponent) robotListComponent: RobotListComponent;

  onRobotCreated(robot: Robot) {
    this.robotListComponent.add(robot);
  }
}
