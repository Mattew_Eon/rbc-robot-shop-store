import {Component} from "@angular/core";
import {Robot} from "../../../model/robot";
import {RobotService} from "../services/robot.service";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: "robot-list",
  templateUrl: "robot-list.component.html",
  animations: [
    trigger("goals", [
      transition(":leave", [
        style({transform: "scale(1)"}),
        animate(".5s ease", style({transform: 'scale(0)'}))
      ]),
      transition(":enter", [
        style({transform: "scale(0)"}),
        animate(".5s ease", style({transform: 'scale(1)'}))
      ])
    ])
  ]
})
export class RobotListComponent {
  public robotList: Robot[] = [];

  constructor(private robotS: RobotService) {
    this.robotS.getRobots().subscribe((robotList: Robot[]) => {
      this.robotList = robotList;
    })
  }

  add(robot: Robot) {
    this.robotList.push(robot);
  }

  deleteRobot(robotId: string) {
    let i: number = this.robotList.findIndex(robot => robot.id == robotId);

    this.robotS.delete(this.robotList[i]).subscribe(() => {
      this.robotList.splice(i, 1);
    });
  }

}
