import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Robot} from "../../../model/robot";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";

@Injectable()
export class RobotService {
  private url = "robot";

  constructor(private http: HttpClient) {
  }

  getRobots(): Observable<Robot[]> {
    return this.http.get<Robot[]>(this.url).pipe(map(data => {
      let robots: Robot[] = [];
      for (let row of data) {
        robots.push(Robot.fromObject(row))
      }
      return robots;
    }));
  }

  getRobot(id: string): Observable<Robot> {
    return this.http.get<Robot>(this.url + "/" + id).pipe(map(Robot.fromObject));
  }

  getByRoute(route: string): Observable<Robot> {
    return this.http.get<Robot>(this.url + "/route/" + route).pipe(map(Robot.fromObject));
  }

  create(robot: Robot): Observable<Robot> {
    return this.http.post<Robot>(this.url, robot).pipe(map(Robot.fromObject));
  }

  update(robot: Robot): Observable<Robot> {
    return this.http.put<Robot>(this.url, robot).pipe(map(Robot.fromObject));
  }

  delete(robot: Robot): Observable<any> {
    return this.http.delete(this.url + "/" + robot.id).pipe();
  }
}
