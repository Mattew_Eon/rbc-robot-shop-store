import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {MatIconModule, MatInputModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterModule} from "@angular/router";
import {RobotViewComponent} from "./components/robot-view.component";
import {RobotCreateComponent} from "./components/robot-create.component";
import {RobotListComponent} from "./components/robot-list.component";
import {RobotService} from "./services/robot.service";
import {RobotHomeComponent} from "./components/robot-home.component";
import {OrderModule} from "ngx-order-pipe";

@NgModule({
  declarations: [
    RobotViewComponent,
    RobotCreateComponent,
    RobotListComponent,
    RobotHomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    MatIconModule,
    MatInputModule,

    OrderModule
  ],
  exports: [
    RobotViewComponent,
    RobotCreateComponent,
    RobotHomeComponent
  ],
  providers: [
    RobotService
  ],
})
export class RobotModule {
}
