import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = localStorage.getItem("token") == undefined ? {} : {token: localStorage.getItem("token")};

    request = request.clone({
      url: environment.backendOrigin + request.url, setHeaders: headers
    });

    return next.handle(request).catch((error: HttpErrorResponse) => {
      console.error("Request " + request.url + " failed :");
      console.error(error.error.text);
      return Observable.create(observer => observer.complete());
    }) as any;
  }
}
