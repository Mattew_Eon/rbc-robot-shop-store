import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {RobotListComponent} from "./modules/Robot/components/robot-list.component";
import {RobotCreateComponent} from "./modules/Robot/components/robot-create.component";
import {RobotViewComponent} from "./modules/Robot/components/robot-view.component";
import {RobotHomeComponent} from "./modules/Robot/components/robot-home.component";

const routes: Routes = [
	{path: '', redirectTo: 'home', pathMatch: 'full'},
	//{path: '', component: IndexComponent, canActivate: [NoAuthGuard]},
	{path: 'home', component: RobotHomeComponent},

	{path: 'robots/list', component: RobotListComponent},
	{path: 'robots/create', component: RobotCreateComponent},
	{path: 'robots/:id/view', component: RobotViewComponent},

	/*{path: '404', component: Handle404Component},
	{path: '**', redirectTo: '404'}*/
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
