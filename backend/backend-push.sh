#!/bin/bash
name="mcm_backend"
ipName="192.168.1.91:32770/$name"

echo "Running mvn clean install -DskipTests..."
mvn clean install -DskipTests

echo "Running docker build -f ./src/main/docker/Dockerfile -t $name ...."
dockerBuildResult=$(docker build -f ./src/main/docker/Dockerfile -t $name .)
tag=$(grep -oP '(?<=built).*?(?=Successfully)' <<< $dockerBuildResult)

echo "Running docker tag $tag $ipName..."
docker tag $tag $ipName

echo "Running docker push $ipName..."
docker push $ipName
$SHELL;