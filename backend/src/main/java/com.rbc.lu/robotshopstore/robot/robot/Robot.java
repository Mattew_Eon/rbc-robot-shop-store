package com.rbc.lu.robotshopstore.robot.robot;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Robot")
public class Robot {
    @Id public String id;

    public float price;
    public String name;
}
