package com.rbc.lu.robotshopstore.robot.robot;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RobotRepository extends MongoRepository<Robot, String> {
	List<Robot> findAll();

	Robot findOne(String id);

	Robot save(Robot robot);

	void delete(Robot robot);
}
