package com.rbc.lu.robotshopstore.robot.robot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rest/robot")
public class RobotController {
	@Autowired
	private RobotService robotService;

	@GetMapping
	public List<Robot> getAll() {
		return robotService.getAll();
	}

	@GetMapping(value = "{id}")
	public Robot getById(@PathVariable("id") String id) { return robotService.getById(id); }


	@PostMapping
	public ResponseEntity<Robot> create(@Valid @RequestBody Robot robot) {
		return new ResponseEntity<>(robotService.create(robot), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Robot> update(@Valid @RequestBody Robot robot) {
		return robotService.update(robot);
	}


	@DeleteMapping(value = "{id}")
	public void delete(@PathVariable("id") String id) {
		robotService.delete(id);
	}
}
