package com.rbc.lu.robotshopstore.robot.robot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RobotService {
	@Autowired
	private RobotRepository robotRepository;

	public List<Robot> getAll() { return robotRepository.findAll(); }

	public Robot getById(String id) { return robotRepository.findOne(id); }

	public Robot create(Robot robot) { return robotRepository.save(robot); }

	public ResponseEntity<Robot> update(Robot robot) {
		Robot searchRobot = getById(robot.id);
		if (searchRobot == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			robot.id = searchRobot.id;
			return new ResponseEntity<>(robotRepository.save(robot), HttpStatus.OK);
		}
	}

	public void delete(Robot robot) {
		robotRepository.delete(robot);
	}

	public void delete(String id) { delete(getById(id)); }
}
