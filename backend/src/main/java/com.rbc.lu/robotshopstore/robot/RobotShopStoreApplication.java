package com.rbc.lu.robotshopstore.robot;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotShopStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RobotShopStoreApplication.class, args);
	}
}
